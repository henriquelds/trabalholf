/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analsintatico;

/**
 *
 * @author Henrique
 */


public class Erro extends Exception {
    private int Column;
    private int Line;
    private String tipo;

    public Erro(int Column, int Line, String tipo) {
        this.Column = Column;
        this.Line = Line;
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "Erro "+tipo+" na linha "+Line+", coluna "+Column;
    }

    public int getColumn() {
        return Column;
    }

    public int getLine() {
        return Line;
    }

    public String getTipo() {
        return tipo;
    }
    
    
    
}
