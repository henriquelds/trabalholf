/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analsintatico;

import java.io.IOException;
import java.util.Stack;

/**
 *
 * @author Henrique
 */
public class AnalSintaticoOld {

    /**
     * @param args the command line arguments
     */
    private AnalisadorLexico al;
    private Stack<Token> tokens;
    
    public AnalSintaticoOld(String str) throws IOException{
        al = new AnalisadorLexico(str);
        tokens = new Stack<Token>();
    }
    
    public boolean I() throws IOException, Erro{
        /*Token tk;
        do{
            tk = removeToken();
            System.out.println(tk+" "+al.getCurrentColumn());
        }while(tk!=Token.EOF);
        return true;*/
        Token tk = removeToken();
        if(Lista_sta(tk)){
            tk = removeToken();
            if(tk == Token.EOF){
                return true;
            }
            else{
                
                Erro e = new Erro(al.getCurrentColumn(), al.getCurrentRow(), "Sintático");
                throw e;
            }
        }
        else{
            
            Erro e = new Erro(al.getCurrentColumn(), al.getCurrentRow(), "Sintático");
            throw e;
        }
    }
    
    private boolean Lista_sta(Token tk) throws IOException, Erro{
        
        if(Sta(tk)){
           tk = removeToken();
           if(Lista_sta(tk)){
               return true;
           }
           else{
               Erro e = new Erro(al.getCurrentColumn(), al.getCurrentRow(), "Sintático");
               throw e;
           }
        }
        else{
            tokens.push(tk);
            return true;
        }
    }
    
    private boolean Sta(Token tk) throws IOException, Erro{
        if(tk == Token.VARIAVEL){
            tk = removeToken();
            if(tk == Token.ATRIBUICAO){
                tk = removeToken();
                if(Exp0(tk)){
                    tk = removeToken();
                    if(tk == Token.DELIMITADOR_PV){
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    private boolean Exp0(Token tk) throws IOException, Erro{
        if(Exp1(tk)){
            tk = removeToken();
            if(Xlinha(tk)){
                return true;
            }
            else{
                Erro e = new Erro(al.getCurrentColumn(), al.getCurrentRow(), "Sintático");
                throw e;
            }
        }
        else{
            Erro e = new Erro(al.getCurrentColumn(), al.getCurrentRow(), "Sintático");
            throw e;
        }
    }
    
    private boolean Xlinha(Token tk) throws IOException, Erro{
        if(tk == Token.EQUIVALENCIA){
            tk = removeToken();
            if(Exp0(tk)){
                return true;
            }
            else{
                Erro e = new Erro(al.getCurrentColumn(), al.getCurrentRow(), "Sintático");
                throw e;
            }
        }
        else{
            tokens.push(tk);
            return true;
        }
    }
    
    private boolean Exp1(Token tk) throws IOException, Erro{
        if(Exp2(tk)){
            tk = removeToken();
            if(Ylinha(tk)){
                return true;
            }
            else{
                
                Erro e = new Erro(al.getCurrentColumn(), al.getCurrentRow(), "Sintático");
                throw e;
            }
        }
        else{
            Erro e = new Erro(al.getCurrentColumn(), al.getCurrentRow(), "Sintático");
            throw e;
        }
    }
    
    private boolean Ylinha(Token tk) throws IOException, Erro{
        if(tk == Token.IMPLICACAO){
            tk = removeToken();
            if(Exp1(tk)){
                return true;
            }
            else{
                
                Erro e = new Erro(al.getCurrentColumn(), al.getCurrentRow(), "Sintático");
                throw e;
            }
        }
        else{
            tokens.push(tk);
            return true;
        }
   }
    
    private boolean Exp2(Token tk) throws IOException, Erro{
        if(Exp4(tk)){
            tk = removeToken();
            if(Exp2linha(tk)){
                return true;
            }
            else{
                Erro e = new Erro(al.getCurrentColumn(), al.getCurrentRow(), "Sintático");
                throw e;
            }
        }
        else{
            Erro e = new Erro(al.getCurrentColumn(), al.getCurrentRow(), "Sintático");
            throw e;
        }
    }
    
    private boolean Exp2linha(Token tk) throws IOException, Erro{
        if(tk == Token.CONJUNCAO){
            tk = removeToken();
            if(Exp4(tk)){
                tk = removeToken();
                if(Exp2linha(tk)){
                    return true;
                }
                else{
                    Erro e = new Erro(al.getCurrentColumn(), al.getCurrentRow(), "Sintático");
                    throw e;
                }
            }
            else{
                Erro e = new Erro(al.getCurrentColumn(), al.getCurrentRow(), "Sintático");
                throw e;
            }
        }
        else{
            tokens.push(tk);
            return true;
        }
    }
    
    private boolean Exp4(Token tk) throws Erro{
        if(tk == Token.VALOR_LOG || tk == Token.VARIAVEL){
            return true;
        }
        else {
            Erro e = new Erro(al.getCurrentColumn(), al.getCurrentRow(), "Sintático");
            throw e;
        }
    }
    
    private Token removeToken() throws IOException, Erro{
        if(!tokens.empty()){
            return tokens.pop();
        }
        else{
            return al.le_token();
        }
        
    }
}
