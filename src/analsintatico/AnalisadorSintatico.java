/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analsintatico;

import java.io.IOException;
import java.util.Stack;

/**
 *
 * @author Henrique
 */
public class AnalisadorSintatico {

    /**
     * @param args the command line arguments
     */
    private AnalisadorLexico al;
    private Stack<Token> tokens;
    
    public AnalisadorSintatico(String str) throws IOException{
        al = new AnalisadorLexico(str);
        tokens = new Stack<Token>();
    }
    
    //I -> Lista_Sta “EOF”
    public boolean I() throws IOException, Erro{
        Token tk = removeToken();
        if(Lista_sta(tk)){
            tk = removeToken();
            if(tk == Token.EOF){
                return true;
            }
            else{
                Erro e = new Erro(al.getCurrentColumn(), al.getCurrentRow(), "Sintático");
                throw e;
            }
        }
        else{
            Erro e = new Erro(al.getCurrentColumn(), al.getCurrentRow(), "Sintático");
            throw e;
        }
    }
    
    //Lista_Sta -> Sta Lista_Sta | vazio
    private boolean Lista_sta(Token tk) throws IOException, Erro{
        
        if(Sta(tk)){
           tk = removeToken();
           if(Lista_sta(tk)){
               return true;
           }
           else{
               return false;
           }
        }
        else{
            tokens.push(tk);
            return true;
        }
    }
    
    //Sta -> “Identificador” “=:=” Exp0 “;” | “Print” ListaP “;”  | “Read” ListaR “;”
    // | “if” Exp0 “{“ Lista_Sta “}”Sta’ 
    private boolean Sta(Token tk) throws IOException, Erro{
        if(tk == Token.VARIAVEL){
            tk = removeToken();
            if(tk == Token.ATRIBUICAO){
                tk = removeToken();
                if(Exp0(tk)){
                    tk = removeToken();
                    if(tk == Token.DELIMITADOR_PV){
                        return true;
                    }
                }
            }
        }else if(tk == Token.PRINT){
            tk = removeToken();
            if( ListaP(tk)){
                tk = removeToken();
                if(tk == Token.DELIMITADOR_PV){
                    return true;
                }
            }
        }else if(tk == Token.READ){
            tk = removeToken();
            if( ListaR(tk)){
                tk = removeToken();
                if(tk == Token.DELIMITADOR_PV){
                    return true;
                }
            }
        }else if(tk == Token.IF){
            tk = removeToken();
            if(Exp0(tk)){
                tk = removeToken();
                if(tk == Token.ACHAVE){
                    tk = removeToken();
                    if(Lista_sta(tk)){
                        tk = removeToken();
                        if(tk == Token.FCHAVE){
                            tk = removeToken();
                            if(StaLinha(tk)){
                                return true;
                            }
                        }
                    }
                }
            }
        }
        
        return false;
    }
    
    //Sta’ -> “else” “{“ Lista_Sta “}”  | vazio
    private boolean StaLinha(Token tk) throws IOException, Erro{
        if(tk == Token.ELSE){
             tk = removeToken();
            if(tk == Token.ACHAVE){
                 tk = removeToken();
                if(Lista_sta(tk)){
                     tk = removeToken();
                    if(tk == Token.FCHAVE){
                         return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            else
                return false;
        }
        else{
            tokens.push(tk);
            return true;
        }
    }
    
    //ListaP -> Exp0ListaP’
    private boolean ListaP(Token tk) throws IOException, Erro{
        if ( Exp0(tk)){
             tk = removeToken();
            if(ListaPLinha(tk)){
               return true;
                
            }
        }
        
        return false;
        
    }
    
    //ListaP’ ->  “,” ListaP  | vazio
    private boolean ListaPLinha(Token tk) throws IOException, Erro{
         if ( tk == Token.DELIMITADOR_V){
           tk = removeToken();
           if(ListaP(tk)){
              return true;                 
            }
            else
                return false;
        }else{
              tokens.push(tk);
              return true;
        }
    }
    
    //ListaR -> “Identificador”ListaR’
    private boolean ListaR(Token tk) throws IOException, Erro{
        if(tk == Token.VARIAVEL){
            tk = removeToken();
            if(ListaRLinha(tk)){
                return true;
            }
        }
        return false;
    }
    
    //ListaR’ -> “,” ListaR    | vazio
    private boolean ListaRLinha(Token tk) throws IOException, Erro{
        if(tk == Token.DELIMITADOR_V){
            tk = removeToken();
            if(ListaR(tk)){
                return true;
            }else
                return false;
        }else{
            tokens.push(tk);
            return true;
        }
    }
    
    //Exp0 -> Exp1Exp0'
    private boolean Exp0(Token tk) throws IOException, Erro{
        if(Exp1(tk)){
            tk = removeToken();
            if(Exp0Linha(tk)){
                return true;
            }
        }
        return false;
    }
    
    //Exp0' -> "<->"Exp0 | vazio
    private boolean Exp0Linha(Token tk) throws IOException, Erro{
        if(tk == Token.EQUIVALENCIA){
            tk = removeToken();
            if( Exp0(tk)){
                return true;
            }else
             return false;
        }else{
            tokens.push(tk);
             return true;
        }
    }
    
    //Exp1 -> Exp2Exp1'
    private boolean Exp1(Token tk) throws IOException, Erro{
        if(Exp2(tk)){
            tk = removeToken();
            if(Exp1Linha(tk)){
                return true;
            }
        }
        return false;
    }
    
    //Exp1' ->"->"Exp1 | vazio
    private boolean Exp1Linha(Token tk) throws IOException, Erro{
        if(tk == Token.IMPLICACAO){
            tk = removeToken();
            if(Exp1(tk)){
                return true;
            }else
                return false;
        }else{
            tokens.push(tk);
            return true;
        }
    }
    
    //Exp2 -> Exp3Exp2'
    private boolean Exp2(Token tk) throws IOException, Erro{
        if(Exp3(tk)){
            tk = removeToken();
            if(Exp2Linha(tk)){
                return true;
            }
        }
        return false;
    }
    
    //Exp2'-> "^"Exp3Exp2' | "v" Exp3Exp2' | vazio
    private boolean Exp2Linha(Token tk) throws IOException, Erro{
        if(tk == Token.CONJUNCAO){
            tk = removeToken();
             if(Exp3(tk)){
                tk = removeToken();
                    if(Exp2Linha(tk)){
                        return true;
                    }else
                        return false;
            }else
                return false;
                          
        }else if(tk == Token.DISJUNCAO){
            tk = removeToken();
            if(Exp3(tk)){
                tk = removeToken();
                if(Exp2Linha(tk)){
                    return true;
                }else
                    return false;
            }else
                 return false;
         
        }else{
            tokens.push(tk);
                return true;
        }
    }
    
    //Exp3 -> Exp4Exp3'        
    private boolean Exp3(Token tk) throws IOException, Erro{
        if(Exp4(tk)){
            tk = removeToken();
            if(Exp3Linha(tk)){
                return true;
            }
        }
        return false;
    }
            
    //Exp3' ->"'"Exp3' | vazio
    private boolean Exp3Linha(Token tk) throws IOException, Erro{
        if(tk == Token.NEGACAO){
            tk = removeToken();
            if(Exp3Linha(tk)){
                return true;
            }else
                return false;
        }else{
            tokens.push(tk);
            return true;
        }
             
    }
    
    //Exp4-> Valor | "(" Exp0 ")"
    private boolean Exp4(Token tk) throws IOException, Erro{
        if(Valor(tk)){
            return true;  
        }else if(tk == Token.APAR){
            tk = removeToken();
            if(Exp0(tk)){
                tk = removeToken();
                if(tk == Token.FPAR){
                    return true;
                }
            }
        }
        return false;
    }
    
    //Valor -> "Identificador" | "Valor Lógico"
    private boolean Valor(Token tk) throws IOException{
        if(tk == Token.VARIAVEL){
            return true;
        }else if(tk == Token.VALOR_LOG){
            return true;
        }
        return false;
    }
    
    private Token removeToken() throws IOException, Erro{
        if(!tokens.empty()){
            return tokens.pop();
        }
        else{
            return al.le_token();
        }
    }   
}
